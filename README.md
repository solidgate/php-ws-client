[![PHP version](https://badge.fury.io/ph/solidgate%2Fws-client.svg)](https://badge.fury.io/ph/solidgate%2Fws-client)

# SolidGate Websocket Client


This library provides client for receive SolidGate payment gateway webhooks.

## Documentation
https://dev.solidgate.com/developers/documentation

## Installation

### With Composer

```
$ composer require solidgate/ws-client
```

```json
{
    "require": {
        "solidgate/ws-client": "~1.0"
    }
}
```

## Usage

```php
<?php

use SolidGate\WebsocketClient\PrintWebhookHandler;
use SolidGate\WebsocketClient\WebhookReceiver;

$receiver = new WebhookReceiver(new PrintWebhookHandler());

$event = ''; // see full list events on https://solidgate.atlassian.net/
$merchant = ''; // pass your merchantId
$privateKey = ''; // pass your privateKey

try {
    $receiver->receive($event, $merchant, $privateKey);
} catch (\SolidGate\WebsocketClient\Exception\ConnectionException $connectionException) {
    var_dump($connectionException->getBody()); // handle connection errors (authentication/validation/... error)
}
```

Run example.php for test usage
```bash
php -f example.php
```
