<?php

require __DIR__ . '/vendor/autoload.php';

use SolidGate\WebsocketClient\PrintWebhookHandler;
use SolidGate\WebsocketClient\WebhookReceiver;

class Logger implements \Psr\Log\LoggerInterface
{
    public function emergency($message, array $context = [])
    {
        var_dump($message, $context);
    }

    public function alert($message, array $context = [])
    {
        var_dump($message, $context);
    }

    public function critical($message, array $context = [])
    {
        var_dump($message, $context);
    }

    public function error($message, array $context = [])
    {
        var_dump($message, $context);
    }

    public function warning($message, array $context = [])
    {
        var_dump($message, $context);
    }

    public function notice($message, array $context = [])
    {
        var_dump($message, $context);
    }

    public function info($message, array $context = [])
    {
        var_dump($message, $context);
    }

    public function debug($message, array $context = [])
    {
        var_dump($message, $context);
    }

    public function log($level, $message, array $context = [])
    {
        var_dump($message, $context);
    }
}

$receiver = new WebhookReceiver(new PrintWebhookHandler(), new Logger());

$event = ''; // see full list events on https://dev.solidgate.com/developers/documentation
$merchant = ''; // pass your merchantId
$privateKey = ''; // pass your privateKey

try {
    $receiver->receive($event, $merchant, $privateKey);
} catch (\SolidGate\WebsocketClient\Exception\ConnectionException $connectionException) {
    var_dump($connectionException->getBody()); // handle connection errors (authentication/validation/... error)
}
