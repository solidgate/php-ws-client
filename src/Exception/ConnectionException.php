<?php

namespace SolidGate\WebsocketClient\Exception;

use Exception;

class ConnectionException extends Exception
{
    /** @var int|null */
    private $statusCode;

    /** @var string|null */
    private $body;

    public function __construct(?int $statusCode, ?string $body)
    {
        parent::__construct("Connection doesn't established");
        $this->statusCode = $statusCode;
        $this->body = $body;
    }

    public function getStatusCode(): ?int
    {
        return $this->statusCode;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }
}
