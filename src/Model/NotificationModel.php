<?php

namespace SolidGate\WebsocketClient\Model;

class NotificationModel
{
    /** @var string|null */
    private $notificationId;

    /** @var string|null */
    private $event;

    /** @var array|null */
    private $body;

    /** @var string|null */
    private $createdAt;

    public function __construct(?string $notificationId, ?string $event, ?array $body, ?string $createdAt)
    {
        $this->notificationId = $notificationId;
        $this->event = $event;
        $this->body = $body;
        $this->createdAt = $createdAt;
    }

    public static function createFromArray(array $array): NotificationModel
    {
        return new NotificationModel(
            $array['notification_id'] ?? null,
            $array['event'] ?? null,
            $array['body'] ?? null,
            $array['created_at'] ?? null
        );
    }

    public function getBody(): ?array
    {
        return $this->body;
    }

    public function isValid(): bool
    {
        return !empty($this->getNotificationId()) &&
            !empty($this->getEvent()) &&
            !empty($this->getCreatedAt());
    }

    public function getNotificationId(): ?string
    {
        return $this->notificationId;
    }

    public function getEvent(): ?string
    {
        return $this->event;
    }

    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }
}
