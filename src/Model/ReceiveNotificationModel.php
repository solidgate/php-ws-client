<?php

namespace SolidGate\WebsocketClient\Model;

class ReceiveNotificationModel
{
    private const METHOD_NOTIFICATION_RECEIVE = 'notification.receive';

    /** @var string */
    private $notificationId;

    /** @var string */
    private $method;

    public function __construct(string $notificationId, string $method)
    {
        $this->notificationId = $notificationId;
        $this->method = $method;
    }

    public static function create(string $notificationId): self
    {
        return new ReceiveNotificationModel($notificationId, self::METHOD_NOTIFICATION_RECEIVE);
    }

    public function serialize(): string
    {
        return (string) json_encode($this->normalize());
    }

    public function normalize(): array
    {
        return [
            'notification_id' => $this->notificationId,
            'method'          => $this->method,
        ];
    }
}
