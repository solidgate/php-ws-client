<?php

namespace SolidGate\WebsocketClient;

use SolidGate\WebsocketClient\Model\NotificationModel;

class PrintWebhookHandler implements WebhookHandlerInterface
{
    public function handle(NotificationModel $model): void
    {
        print "notification: \n";

        printf("notification_id: %s \n", $model->getNotificationId());
        printf("event: %s \n", $model->getEvent());
        printf("created_at: %s \n", $model->getCreatedAt());
        printf("body: %s \n", json_encode($model->getBody()));

        print "\n";
    }

    public function isHandled(NotificationModel $model): bool
    {
        return true;
    }
}
