<?php

namespace SolidGate\WebsocketClient;

class Util
{
    public static function generateSignature(string $data, string $merchantId, string $privateKey): string
    {
        return base64_encode(
            hash_hmac(
                'sha512',
                $merchantId . $data . $merchantId,
                $privateKey
            )
        );
    }
}
