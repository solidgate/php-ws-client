<?php

namespace SolidGate\WebsocketClient;

use SolidGate\WebsocketClient\Model\NotificationModel;

interface WebhookHandlerInterface
{
    public function handle(NotificationModel $model): void;

    public function isHandled(NotificationModel $model): bool;
}
