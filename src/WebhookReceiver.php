<?php

namespace SolidGate\WebsocketClient;

use Amp\Http\Client\Connection\UnprocessedRequestException;
use Amp\Loop;
use Amp\Socket\ConnectException;
use Amp\Websocket\Client\Connection;
use Amp\Websocket\Client\ConnectionException;
use Amp\Websocket\ClosedException;
use Amp\Websocket\Message;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use SolidGate\WebsocketClient\Model\NotificationModel;
use SolidGate\WebsocketClient\Model\ReceiveNotificationModel;
use function Amp\Websocket\Client\connect;

class WebhookReceiver
{
    public const WEBHOOK_URL = 'wss://events.solidgate.com';
    private const PATH_SUBSCRIBE = '/api/v1/events/subscribe';
    private const MAX_CONNECTION_ATTEMPT_NUMBER = 30;
    private const TIMEOUT_BEFORE_RECONNECT_SEC = 3;

    /** @var WebhookHandlerInterface */
    private $webhookHandler;

    /** @var LoggerInterface */
    private $logger;

    /** @var string */
    private $webhookServerUrl;

    /** @var int */
    private $maxConnectionAttempNumber;

    /** @var int */
    private $timeoutBeforeReconnectSec;

    public function __construct(WebhookHandlerInterface $webhookHandler, LoggerInterface $logger = null)
    {
        $this->webhookServerUrl = self::WEBHOOK_URL;
        $this->maxConnectionAttempNumber = self::MAX_CONNECTION_ATTEMPT_NUMBER;
        $this->timeoutBeforeReconnectSec = self::TIMEOUT_BEFORE_RECONNECT_SEC;

        $this->webhookHandler = $webhookHandler;
        $this->logger = $logger ?? new NullLogger();
    }

    /**
     * @param string $event
     * @param string $merchantId
     * @param string $privateKey
     *
     * @throws Exception\ConnectionException
     */
    public function receive(string $event, string $merchantId, string $privateKey): void
    {
        $this->safeReceive($event, $merchantId, $privateKey, 0);
    }

    /**
     * @param string $event
     * @param string $merchantId
     * @param string $privateKey
     * @param int    $connectionAttemptNumber
     *
     * @throws Exception\ConnectionException
     */
    public function safeReceive(
        string $event,
        string $merchantId,
        string $privateKey,
        int $connectionAttemptNumber
    ): void {
        try {
            Loop::run(function () use ($event, $merchantId, $privateKey, &$connectionAttemptNumber) {
                try {
                    /** @var Connection $connection */
                    $connection = yield connect($this->buildUrl($event, $merchantId, $privateKey));
                } catch (ConnectionException $throwable) {
                    throw new Exception\ConnectionException(
                        $throwable->getResponse()->getStatus(),
                        array_values((array) $throwable->getResponse()->getBody()->buffer())[1] ?? null
                    );
                }

                $connectionAttemptNumber = 0;
                while ($message = yield $connection->receive()) {
                    $this->logger->debug('receiver.memory_usage', [
                        'memory_usage_K' => $this->getMemoryUsage(),
                    ]);

                    /** @var Message $message */
                    $payload = yield $message->buffer();

                    $notificationModel = NotificationModel::createFromArray(json_decode($payload, true));
                    if (!$notificationModel->isValid()) {
                        $this->logger->warning('receiver.receive_invalid_message', [
                            'message_body' => $payload,
                        ]);

                        continue;
                    }

                    $this->logger->debug('receiver.receive_message', [
                        'notification_id' => $notificationModel->getNotificationId(),
                        'event_name'      => $notificationModel->getEvent(),
                    ]);

                    if ($this->handleMessage($notificationModel)) {
                        $this->logger->debug('receiver.handle_message', [
                            'notification_id' => $notificationModel->getNotificationId(),
                            'event_name'      => $notificationModel->getEvent(),
                        ]);

                        $connection->send(ReceiveNotificationModel::create(
                            (string) $notificationModel->getNotificationId()
                        )->serialize());
                    }
                }
            });
        } catch (UnprocessedRequestException|ConnectException|ClosedException $throwable) {
            $this->logger->info('receiver.reconnect', [
                'attempt_number'    => $connectionAttemptNumber,
                'exception_class'   => get_class($throwable),
                'exception_message' => $throwable->getMessage(),
            ]);
            if ($connectionAttemptNumber >= $this->maxConnectionAttempNumber) {
                $this->logger->error($throwable->getMessage(), [
                    'trace' => $throwable->getTraceAsString(),
                ]);

                throw new Exception\ConnectionException(
                    500,
                    'Houston we have a problem! Number of connection attempts is exceeded. Contact Solid support :('
                );
            }
            sleep($this->timeoutBeforeReconnectSec);
            $connectionAttemptNumber++;
            $this->safeReceive($event, $merchantId, $privateKey, $connectionAttemptNumber);
        }
    }

    private function buildUrl(string $event, string $merchantId, string $privateKey): string
    {
        $queryParams = ['event' => $event, 'merchant' => $merchantId];

        $query = http_build_query($queryParams);
        $signature = Util::generateSignature($query, $merchantId, $privateKey);

        return sprintf("%s%s?%s&signature=%s", $this->webhookServerUrl, self::PATH_SUBSCRIBE, $query, $signature);
    }

    private function getMemoryUsage(): string
    {
        $memory = memory_get_usage() / 1024;

        return number_format($memory, 3);
    }

    private function handleMessage(NotificationModel $notificationModel): bool
    {
        try {
            $this->webhookHandler->handle($notificationModel);

            return $this->webhookHandler->isHandled($notificationModel);
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable->getMessage(), [
                'trace' => $throwable->getTraceAsString(),
            ]);
        }

        return false;
    }

    public function withWebhookUrl(string $webhookUrl): self
    {
        $this->webhookServerUrl = rtrim($webhookUrl, '/');

        return $this;
    }

    public function withMaxConnectionAttempNumber(int $maxConnectionAttempNumber): self
    {
        $this->maxConnectionAttempNumber = $maxConnectionAttempNumber;

        return $this;
    }

    public function withTimeoutBeforeReconnectSec(int $timeoutBeforeReconnectSec): self
    {
        $this->timeoutBeforeReconnectSec = $timeoutBeforeReconnectSec;

        return $this;
    }
}
