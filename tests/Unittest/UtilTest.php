<?php

namespace SolidGate\WebsocketClient;

use PHPUnit\Framework\TestCase;

class UtilTest extends TestCase
{
    public function testGenerateSignature()
    {
        $signature = Util::generateSignature('merchant=merchant&event=event_name', 'merchant', 'privateKey');
        $this->assertEquals(
            'MmY0NzBlNGE0OTlkNTk3ZGIxMjI5OTM0OWIyYjFkNmY0NDBhNmE2NTAzODhjNjc0Y2M4MWFkZDI0YWFjOTA3M2ZhYjJiMjBhMWZmMmVkZTA0NzJmNTBiMzlmY2Y3YjY4MDYyMDg2NDlmMDQxNTRjNTFlYzE1YjkzNzQ4ODc0YmE=',
            $signature
        );
    }
}
